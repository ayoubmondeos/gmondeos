<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.or...ses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author PrestaShop SA <contact@prestashop.com-->
* @copyright 2007-2012 PrestaShop SA
* @version Release: $Revision: 14944 $
* @license http://opensource.or...ses/osl-3.0.php Open Software License (OSL 3.0)
* International Registered Trademark &amp; Property of PrestaShop SA
*/

class Product extends ProductCore
{
/**
* Get product price
*
* @param integer $id_product Product id
* @param boolean $usetax With taxes or not (optional)
* @param integer $id_product_attribute Product attribute id (optional).
* If set to false, do not apply the combination price impact. NULL does apply the default combination price impact.
* @param integer $decimals Number of decimals (optional)
* @param integer $divisor Useful when paying many time without fees (optional)
* @param boolean $only_reduc Returns only the reduction amount
* @param boolean $usereduc Set if the returned amount will include reduction
* @param integer $quantity Required for quantity discount application (default value: 1)
* @param boolean $forceAssociatedTax DEPRECATED - NOT USED Force to apply the associated tax. Only works when the parameter $usetax is true
* @param integer $id_customer Customer ID (for customer group reduction)
* @param integer $id_cart Cart ID. Required when the cookie is not accessible (e.g., inside a payment module, a cron task...)
* @param integer $id_address Customer address ID. Required for price (tax included) calculation regarding the guest localization
* @param variable_reference $specificPriceOutput.
* If a specific price applies regarding the previous parameters, this variable is filled with the corresponding SpecificPrice object
* @param boolean $with_ecotax insert ecotax in price output.
* @return float Product price
*/
public static function getPriceStatic($id_product, $usetax = true, $id_product_attribute = null, $decimals = 6, $divisor = null,
$only_reduc = false, $usereduc = true, $quantity = 1, $force_associated_tax = false, $id_customer = null, $id_cart = null,
$id_address = null, $specific_price_output = null, $with_ecotax = true, $use_group_reduction = true, Context $context = null,
$use_customer_price = true)
{
    if (!$context)
    $context = Context::getContext();

    $cur_cart = $context->cart;

    if ($divisor !== null)
    Tools::displayParameterAsDeprecated('divisor');

    if (!Validate::isBool($usetax) || !Validate::isUnsignedId($id_product))
    die(Tools::displayError());
    // Initializations
    $id_group = (isset($context->customer) ? $context->customer->id_default_group : _PS_DEFAULT_CUSTOMER_GROUP_);

    // If there is cart in context or if the specified id_cart is different from the context cart id
    if (!is_object($cur_cart) || (Validate::isUnsignedInt($id_cart) && $id_cart && $cur_cart->id != $id_cart))
    {
    /*
    * When a user (e.g., guest, customer, Google...) is on PrestaShop, he has already its cart as the global (see /init.php)
    * When a non-user calls directly this method (e.g., payment module...) is on PrestaShop, he does not have already it BUT knows the cart ID
    * When called from the back office, cart ID can be inexistant
    */
    if (!$id_cart && !isset($context->employee))
    die(Tools::displayError());
    $cur_cart = new Cart($id_cart);
    // Store cart in context to avoid multiple instantiations in BO
    if (!Validate::isLoadedObject($context->cart))
    $context->cart = $cur_cart;
    }

    $cart_quantity = 0;
    if ((int)$id_cart)
    {
    $condition = '';
    $cache_name = (int)$id_cart.'_'.(int)$id_product;
    if (!isset(self::$_cart_quantity[$cache_name]) || self::$_cart_quantity[$cache_name] != (int)$quantity)
    self::$_cart_quantity[$cache_name] = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('
    SELECT SUM(`quantity`)
    FROM `'._DB_PREFIX_.'cart_product`
    WHERE `id_product` = '.(int)$id_product.'
    AND `id_cart` = '.(int)$id_cart);
    $cart_quantity = self::$_cart_quantity[$cache_name];
    }

    $id_currency = (int)Validate::isLoadedObject($context->currency) ? $context->currency->id : Configuration::get('PS_CURRENCY_DEFAULT');

    // retrieve address informations
    $id_country = (int)$context->country->id;
    $id_state = 0;
    $zipcode = 0;

    if (!$id_address)
    $id_address = $cur_cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')};

    if ($id_address)
    {
    $address_infos = Address::getCountryAndState($id_address);
    if ($address_infos['id_country'])
    {
    $id_country = (int)$address_infos['id_country'];
    $id_state = (int)$address_infos['id_state'];
    $zipcode = $address_infos['postcode'];
    }
    }
    else if (isset($context->customer->geoloc_id_country))
    {
    $id_country = (int)$context->customer->geoloc_id_country;
    $id_state = (int)$context->customer->id_state;
    $zipcode = (int)$context->customer->postcode;
    }

    if (Tax::excludeTaxeOption())
    $usetax = false;

    if ($usetax != false
    && !empty($address_infos['vat_number'])
    && $address_infos['id_country'] != Configuration::get('VATNUMBER_COUNTRY')
    && Configuration::get('VATNUMBER_MANAGEMENT'))
    $usetax = false;

    if (is_null($id_customer) && Validate::isLoadedObject($context->customer))
    $id_customer = $context->customer->id;

    include_once(dirname(__FILE__).'../../../modules/categoryquantitydiscount/categoryquantitydiscount.php');
    $cqd = new CategoryQuantityDiscount();

    return Product::priceCalculation(
        $context->shop->id,
        $id_product,
        $id_product_attribute,
        $id_country,
        $id_state,
        $zipcode,
        $id_currency,
        $id_group,
        //$cart_quantity,
        $cqd->getRealQuantity($id_product,
        $id_cart,
        $quantity),
        $usetax,
        $decimals,
        $only_reduc,
        $usereduc,
        $with_ecotax,
        $specific_price_output,
        $use_group_reduction,
        $id_customer,
        $use_customer_price,
        $id_cart,
        $cqd->getRealQuantity($id_product,
        $id_cart,
        $quantity)
    );
}
}